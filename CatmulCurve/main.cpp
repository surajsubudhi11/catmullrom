
// system headers
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <vector>

#include "Shaders.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "filesystem.h"

//! Screen Resize Callback Function
void screen_resize_callback(GLFWwindow* a_window, int a_width, int a_height);
//! For Inputs Function(ESC for quit)
void process_input(GLFWwindow *a_window);

//=============== Type defs ======================

typedef std::vector<float> VertexData;
typedef std::vector<unsigned int> IndicesData;

//================================================

//=============== Function Prototypes ======================
void allocate_gpu_memory(const VertexData &a_vert_data, const IndicesData &a_indices_data, unsigned int& a_vao, const int& a_size);

void load_and_create_texture(const unsigned int a_vao, unsigned int& a_texture_id, const std::string& a_filepath);

void get_vertex_data(const  VertexData &a_control_points, VertexData &a_vertices, IndicesData &a_indices,
                    VertexData &a_vertices_face, IndicesData &a_indices_face, const unsigned int& a_divisions, const float& a_width);

//==========================================================
// Global Settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

const unsigned int DIVISIONS = 8;
//const float EXTRUDE_WIDTH = 0.05f;

void initialize_glfw()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
}

/**
 * \brief Setting up the window
 * \param a_title - Title of the window
 * \param a_height - Height for the window
 * \param a_width - Width for the window
 * \return - GLFW window context
 */
GLFWwindow* setup_window(const char* a_title, unsigned int a_height, unsigned int a_width)
{
    // glfw window creation
    // --------------------
    const auto window = glfwCreateWindow(a_width, a_height, a_title, nullptr, nullptr);
    if (window == nullptr)
    {
        std::cerr << "Failed to create GLFW window" << "\n";
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, screen_resize_callback);

    return window;
}

void initialize_glew()
{
    glewExperimental = true; // Needed in core profile
    if (glewInit() != GLEW_OK) 
    {
        std::cerr << "Failed to initialize GLEW" << "\n";
        return;
    }
}

/**
 * \brief - Build the Shader for and compile of given type
 * \param a_type - Type of shader (Vertex or Fragment Shader)
 * \param a_shader_id - the Shader unique ID
 * \param a_shader_source - Shader source
 */
void build_shader(const GLenum a_type, int& a_shader_id, const char* a_shader_source)
{
    a_shader_id = glCreateShader(a_type);
    glShaderSource(a_shader_id, 1, &a_shader_source, nullptr);
    glCompileShader(a_shader_id);
    // check for shader compile errors
    int success;
    char infoLog[512];
    glGetShaderiv(a_shader_id, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(a_shader_id, 512, nullptr, infoLog);
        std::cerr << "shader " << a_shader_id << " compile failed \n" << infoLog << "\n";
    }
}

/**
 * \brief - Link the given vertex shader and fragment shader for particular shader program
 * \param a_vertex_shader - (Input) Unique vertex shader ID
 * \param a_fragment_shader - (Input) Unique fragment shader ID
 * \param a_shader_program_id  - (Output) Create a Shader program and link both vertex shader and fragment shader and return shader program unique ID
 */
void link_shader(const int& a_vertex_shader, const int& a_fragment_shader, int& a_shader_program_id)
{
    a_shader_program_id = glCreateProgram();
    glAttachShader(a_shader_program_id, a_vertex_shader);
    glAttachShader(a_shader_program_id, a_fragment_shader);
    glLinkProgram(a_shader_program_id);
    // check for linking errors
    int success;
    char infoLog[512];
    glGetProgramiv(a_shader_program_id, GL_LINK_STATUS, &success);
    if (!success) 
    {
        glGetProgramInfoLog(a_shader_program_id, 512, nullptr, infoLog);
        std::cerr << "Shader program failed \n" << infoLog << "\n";
    }
}


int main(int argc, char* argv[])
{
    auto number_of_control_points = 0;
    char* filepath;
    float face_width = 0.1f;
    char *p;
    if(argc == 4){
        number_of_control_points = strtol(argv[1], &p, 10);
        if(number_of_control_points < 4)
        {
            std::cerr << "4 Control Points are required to create a catmul rom spline. \n Error!!! \n Exiting!!! \n";
            system("pause");
            exit(1);
        }
        //divisions = strtol(argv[2], &p, 10);
        filepath = argv[2];
        face_width = std::stof(argv[3]);
        std::cout << "Num of Control Points : " << number_of_control_points << "\n";
        std::cout << "Texture file Path : " << filepath << "\n";
        std::cout << "Width : " << face_width << "\n";
    }else
    {
        std::cerr << "Plase provide the number of control point, and texture file path, width for the face. \n Exiting!! \n";
        system("pause");
        exit(1);
    }

    VertexData controlPoints;
    if(number_of_control_points >= 4)
    {
        std::cout << "Please Enter " << 2 * number_of_control_points << " numbers value between -1 to 1. Press enter after each number Entered" << "\n";
        
        for (auto i = 0; i < 2 * number_of_control_points; i++)
        {
            float val;
            std::cin >> val;
            controlPoints.push_back(val);
        }
    }

    initialize_glfw();
    const auto window = setup_window("Catmull Rom Spline", SCR_HEIGHT, SCR_WIDTH);
    initialize_glew();

    
    //! Curve Data
    VertexData vertices;
    IndicesData indices;

    //! Control Points Data
   /* const VertexData controlPoints = 
    { 
       -0.50f,  0.50f,
       -0.25f,  0.25f,
        0.10f,  0.55f,
        0.50f,  0.25f,
        1.50f,  1.00f,
        2.00f, -2.00f
    };*/

    IndicesData controlPointsIndices = { 0, 1, 2, 3, 4, 5 };

    //! Extruded Face Data
    VertexData faceVertices;
    IndicesData faceIndices;

    get_vertex_data(controlPoints, vertices, indices, faceVertices, faceIndices, DIVISIONS, face_width);

    // Shader Parameters
    
    //! Curve Shader program
    int shaderProgramIdCurve;
    int vertexShaderId, fragementShaderId;
    build_shader(GL_VERTEX_SHADER, vertexShaderId, vertex_shader_source);
    build_shader(GL_FRAGMENT_SHADER, fragementShaderId, fragment_shader_source);
    link_shader(vertexShaderId, fragementShaderId, shaderProgramIdCurve);

    //! Allocating memory to GPU for Curve Points
    unsigned int vaoCurve;
    allocate_gpu_memory(vertices, indices, vaoCurve, 3);

    //! Extrude Faces Shader program
    int shaderProgramIdFaces;
    int vertexShaderFaces, fragmentShaderFaces;
    build_shader(GL_VERTEX_SHADER, vertexShaderFaces, vertex_shader_control_face);
    build_shader(GL_FRAGMENT_SHADER, fragmentShaderFaces, fragment_shader_face);
    link_shader(vertexShaderFaces, fragmentShaderFaces, shaderProgramIdFaces);

    //! Allocating memory to GPU for Extruded Geo
    unsigned int vaoFace;
    unsigned int textureID;
    const std::string textureFilePath = filepath;//"D:/Code_Stuff/OpenGL/catmullrom/CatmulCurve/bricks2.jpg";
    allocate_gpu_memory(faceVertices, faceIndices, vaoFace, 3);
    load_and_create_texture(vaoFace, textureID, textureFilePath);

    //! Points Shader program
    int shaderProgramIdPoints;
    int vertexShaderControlPoints, fragmentShaderControlPoints;
    build_shader(GL_VERTEX_SHADER, vertexShaderControlPoints, vertex_shader_control_points);
    build_shader(GL_FRAGMENT_SHADER, fragmentShaderControlPoints, fragment_shader_control_points);
    link_shader(vertexShaderControlPoints, fragmentShaderControlPoints, shaderProgramIdPoints);

    //! Allocating memory to GPU for Control Points
    unsigned int vaoControlPoint;
    allocate_gpu_memory(controlPoints, controlPointsIndices, vaoControlPoint, 2);

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // input
        process_input(window);

        // render
        glClearColor(0.f, 0.f, 0.f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // render container
        //! Render Call for the Extruded Face
        glBindVertexArray(vaoFace);
        // bind Texture
        glBindTexture(GL_TEXTURE_2D, textureID);
        //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glUseProgram(shaderProgramIdFaces);
        glDrawElements(GL_TRIANGLE_STRIP, faceIndices.size(), GL_UNSIGNED_INT, nullptr);
        
        //! Render Call for the Curve
        glBindVertexArray(vaoCurve);
        glUseProgram(shaderProgramIdCurve);
        glDrawElements(GL_LINE_STRIP, indices.size(), GL_UNSIGNED_INT, nullptr);
        
        //! Render Call for the Control Points
        glBindVertexArray(vaoControlPoint);
        glUseProgram(shaderProgramIdPoints);
        glPointSize(3.0f);
        glDrawElements(GL_POINTS, controlPointsIndices.size(), GL_UNSIGNED_INT, nullptr);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glDeleteVertexArrays(1, &vaoCurve);
    /*glDeleteBuffers(1, &vboCurve);
    glDeleteBuffers(1, &iboCurve);*/

    glfwTerminate();
    return 0;    
}

void screen_resize_callback(GLFWwindow* a_window, const int a_width, const int a_height)
{
    glViewport(0, 0, a_width, a_height);
}

void process_input(GLFWwindow *a_window)
{
    if (glfwGetKey(a_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(a_window, true);
}


/**
 * \brief - Computes the cubic hermite interpolation
 * \param a_start_val - (Input) First point for the interpolation to start (t = 0)
 * \param a_end_val   - (Input) Last point for the interpolation to end (t = 1)
 * \param a_tangent_start - (Input) Tangent at first point for the interpolation to start (t = 0)
 * \param a_tangent_end   - (Input) Tangent at last point for the interpolation to end (t = 1)
 * \param a_t - (Input) Compute the cubic hermite interpolation at give t. (t = a_t)
 * \param a_tangent_t - (Output) Outputs the tangent at the interpolated point
 * \return - (Output) the interpolated point at t = a_t.
 */
float cubic_hermite_interpolation( const float a_start_val, const float a_end_val, const float a_tangent_start, const float a_tangent_end, const float a_t, float& a_tangent_t)
{

    a_tangent_t = (((6 * a_t * a_t) + (-6 * a_t)    ) * a_start_val
                + ((-6 * a_t * a_t) + ( 6 * a_t)    ) * a_end_val
                + (( 3 * a_t * a_t) + (-4 * a_t) + 1) * a_tangent_start
                + (( 3 * a_t * a_t) + (-2 * a_t)    ) * a_tangent_end);

    return (( (  2 * a_t * a_t * a_t) + (-3 * a_t * a_t)   + 1) * a_start_val 
            + ((-2 * a_t * a_t * a_t) + ( 3 * a_t * a_t)      ) * a_end_val 
            + (     (a_t * a_t * a_t) + (-2 * a_t * a_t) + a_t) * a_tangent_start
            + (     (a_t * a_t * a_t) + (-1 * a_t * a_t)      ) * a_tangent_end);
}

/**
 * \brief  - Computes the Catmull-Rom Splines interpolations at given t and give the interpolated point and its tangent.
 * \param a_p0 - (Input) Control Point p0
 * \param a_p1 - (Input) Control Point p1
 * \param a_p2 - (Input) Control Point p2
 * \param a_p3 - (Input) Control Point p3
 * \param a_t  - (Input) Compute the Catmull-Rom Splines interpolations at give t. (t = a_t)  
 * \param a_tangent_t - (Output) Gives the tangent of the curve at the interpolated point.
 * \return - (Output) Gives the interpolated point at t = a_t.
 */
float catmull_rom_interpolation(const float a_p0, const float a_p1, const float a_p2, const float a_p3, const float a_t, float& a_tangent_t)
{
    const auto tangent1 = (a_p2 - a_p0) / 2.0f;
    const auto tangent2 = (a_p3 - a_p1) / 2.0f;
    return  cubic_hermite_interpolation(a_p1, a_p2, tangent1, tangent2, a_t, a_tangent_t);
}

/**
 * \brief Compute the normal at a point on curve given tangents.
 * \param a_tangent_x - (Input) x coordinate of the given tangent
 * \param a_tangent_y - (Input) y coordinate of the given tangent
 * \param a_normal_x - (Output) x coordinate of the normal computed
 * \param a_normal_y - (Output) y coordinate of the normal computed
 */
void compute_normal(const float& a_tangent_x, const float& a_tangent_y, float& a_normal_x, float& a_normal_y)
{
    // Cross product of tangent vector with the Up vector
    a_normal_x =  a_tangent_y / sqrtf(powf(a_tangent_x, 2) + powf(a_tangent_y, 2));
    a_normal_y = -a_tangent_x / sqrtf(powf(a_tangent_x, 2) + powf(a_tangent_y, 2));
}

/**
 * \brief Get the Vertex data and Indices Data for the Curve and Extruded faces data from four points  
 * \param a_x0 (Input) Point 0 - X Val \param a_y0 (Input) Point 0 - Y Val
 * \param a_x1 (Input) Point 1 - X Val \param a_y1 (Input) Point 1 - Y Val
 * \param a_x2 (Input) Point 2 - X Val \param a_y2 (Input) Point 2 - Y Val
 * \param a_x3 (Input) Point 3 - X Val \param a_y3 (Input) Point 3 - Y Val
 * \param a_vertices            - (Output) The Vertex data for the Curve
 * \param a_indices             - (Output) The Indices data for the Curve
 * \param a_vertices_face       - (Output) The Vertex data for the Extruded Face
 * \param a_indices_face        - (Output) The Indices data for the Extruded Face
 * \param a_divisions   - (Input) The subdivisions for each curve
 * \param a_width       - (Input) The width of the Extrude face
 */
void get_vertex_data(
    const float a_x0, const float a_y0,
    const float a_x1, const float a_y1,
    const float a_x2, const float a_y2,
    const float a_x3, const float a_y3,
    VertexData &a_vertices,      IndicesData &a_indices,
    VertexData &a_vertices_face, IndicesData &a_indices_face,
    const unsigned int a_divisions, const float a_width)
{
    float tangentX, tangentY;
    float normalX, normalY;

    const auto indicesOffsetCurve = a_indices.size();
    const auto indicesOffsetFace = a_indices_face.size();

    for (unsigned int i = 0; i <= a_divisions; i++)
    {
        const auto t = (i) * (1.0f / a_divisions);

        // Catmull Rom interpolation for each coordinates and computes tangents at that point
        auto xVal = catmull_rom_interpolation(a_x0, a_x1, a_x2, a_x3, t, tangentX); // x
        auto yVal = catmull_rom_interpolation(a_y0, a_y1, a_y2, a_y3, t, tangentY); // y

        // Computing Unit Normal from tangents
        compute_normal(tangentX, tangentY, normalX, normalY);

        // Curve vertex Coordinates
        a_vertices.push_back(xVal); //x
        a_vertices.push_back(yVal); //y
        a_vertices.push_back(0.0f); //z
        // Curve UV Coordinates
        a_vertices.push_back(0.0f);
        a_vertices.push_back(0.0f);

        // Extrude geo vertex Coordinates upwards
        a_vertices_face.push_back(xVal + (normalX * a_width)); // x
        a_vertices_face.push_back(yVal + (normalY * a_width)); // y
        a_vertices_face.push_back(0.0f);                       // z
        // Extrude geo UV Coordinates upwards
        a_vertices_face.push_back(t);
        a_vertices_face.push_back(1.0f);

        // Extrude geo vertex Coordinates downwards
        a_vertices_face.push_back(xVal - (normalX * a_width)); // x
        a_vertices_face.push_back(yVal - (normalY * a_width)); // y
        a_vertices_face.push_back(0.0f);                       // z
        // Extrude geo UV Coordinates upwards
        a_vertices_face.push_back(t);
        a_vertices_face.push_back(0.0f);

        // Pushing indices for Face
        a_indices_face.push_back((2 * i) + indicesOffsetFace);
        a_indices_face.push_back((2 * i) + indicesOffsetFace + 1);
        
        // Pushing indices for Curve
        a_indices.push_back(i + indicesOffsetCurve);
    }
}


/**
 * \brief Get the Vertex data and Indices Data for the Curve and Extruded faces data from the control points with specified divison and width for faces.
 * \param a_control_points - (Input) The Control Points for the Curve
 * \param a_vertices - (Output) The Vertex data for the Curve
 * \param a_indices - (Output) The Indices data for the Curve
 * \param a_vertices_face - (Output) The Vertex data for the Extruded Face
 * \param a_indices_face - (Output) The Indices data for the Extruded Face
 * \param a_divisions - (Input) The subdivisions for each curve
 * \param a_width - (Input) The width of the Extrude face
 */
void get_vertex_data(const  VertexData &a_control_points, VertexData &a_vertices, IndicesData &a_indices,
    VertexData &a_vertices_face, IndicesData &a_indices_face, const unsigned int& a_divisions, const float& a_width)
{
    a_vertices.clear();
    a_indices.clear();

    const auto numOfCurves = (a_control_points.size() / 2) - 3;

    for(unsigned int i = 0; i < (2 * numOfCurves); i += 2)
    {
        // Getting four Conjucative points for getting single catmull rom curve
        const auto px0 = a_control_points[i];       // point0 x
        const auto py0 = a_control_points[i + 1];   // point0 y

        const auto px1 = a_control_points[i + 2];   // point1 x
        const auto py1 = a_control_points[i + 3];   // point1 y

        const auto px2 = a_control_points[i + 4];   // point2 x
        const auto py2 = a_control_points[i + 5];   // point2 y

        const auto px3 = a_control_points[i + 6];   // point3 x
        const auto py3 = a_control_points[i + 7];   // point3 y
        
        get_vertex_data(px0, py0, px1, py1, px2, py2, px3, py3, a_vertices, a_indices, a_vertices_face, a_indices_face, a_divisions, a_width);
    }
}

/**
 * \brief Allocate memory into the GPU for give vertex and indices data
 * \param a_vert_data - (Input) The Vertex position and texture cooordinate data
 * \param a_indices_data - (Input) The index data for allocating memory into IBO (Index Buffer Object)
 * \param a_vao - (Output) The Vertex Array Object ID where the input data are alloted.
 * \param a_size - (Input) Number of floating point to describe  position.
 */
void allocate_gpu_memory(const VertexData &a_vert_data, const IndicesData &a_indices_data, unsigned int& a_vao, const int& a_size)
{
    unsigned int vbo, ibo;
    glGenVertexArrays(1, &a_vao);
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ibo);

    glBindVertexArray(a_vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, a_vert_data.size() * sizeof(float), a_vert_data.data(), GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, a_indices_data.size() * sizeof(unsigned int), a_indices_data.data(), GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, a_size, GL_FLOAT, GL_FALSE, (a_size + 2) * sizeof(float), static_cast<void*>(nullptr));
    glEnableVertexAttribArray(0);

    // texture coordinate attibutes
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, (a_size + 2) * sizeof(float), (void*)(a_size * sizeof(float)));
    glEnableVertexAttribArray(1);
}


/**
 * \brief Load and create texture by allocating data in gpu for given vertex Array objects
 * \param a_vao - (Input) The Vertex Array object where the texture to be bind
 * \param a_texture_id - (Output) The unique texture ID 
 * \param a_filepath - (Input) The texture file path (not relative path)
 */
void load_and_create_texture(const unsigned int a_vao, unsigned int& a_texture_id, const std::string& a_filepath)
{
    glBindVertexArray(a_vao);
    glGenTextures(1, &a_texture_id);
    glBindTexture(GL_TEXTURE_2D, a_texture_id); 
    
    // Texture wrapping parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    
    // Texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    int width, height, nr_channels;
    unsigned char *data = stbi_load(a_filepath.c_str(), &width, &height, &nr_channels, 0);
    if (data)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        std::cout << "Failed to load texture" << std::endl;
    }
    stbi_image_free(data);
    glBindVertexArray(0);
}