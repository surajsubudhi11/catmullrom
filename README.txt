* I have given the sln file into the zip with all the library linking for openGL.
* As its dynamically linked for glew you need to paste the glew32.dll file into your exe file folder located as per the configuration you are running.

// Notes
* I have not used any vector class as I was primarily focusing on the Spline implementation.
* I have not added any camera setup so all my input coordinates need be between -1.0 to 1.0.
* So the width for face need to be small try to keep it as between (0 to 0.1) for better result
* you need to pass full path of the texture not relative path.
* To add arguments in visual studio go to project properties >> Configuration Properties in left side and the into >> Debugging section >> Command Arguments.



